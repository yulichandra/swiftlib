//
//  SwiftLib.swift
//  SwiftLib
//
//  Created by Yuli Chandra on 02/07/20.
//  Github: https://github.com/yulichandra
//
//  Copyright © 2020 Yuli Chandra. All rights reserved.
//

import Foundation
public final class SwiftyLib {

    let name = "SwiftyLib"
    
    public func add(a: Int, b: Int) -> Int {
        return a + b
    }
    
    public func sub(a: Int, b: Int) -> Int {
        return a - b
    }
    
}
