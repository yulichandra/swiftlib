//
//  SwiftLibTests.swift
//  SwiftLib
//
//  Created by Yuli Chandra on 02/07/20.
//  Github: https://github.com/yulichandra
//
//  Copyright © 2020 Yuli Chandra. All rights reserved.
//

import XCTest
@testable import SwiftLib

class SwiftLibTests: XCTestCase {

    var swiftyLib: SwiftyLib!

    override func setUp() {
        swiftyLib = SwiftyLib()
    }

    func testAdd() {
        XCTAssertEqual(swiftyLib.add(a: 1, b: 1), 2)
    }
    
    func testSub() {
        XCTAssertEqual(swiftyLib.sub(a: 2, b: 1), 1)
    }

}
